package Task_1;

public class Text {
    public static String text = "\n" +
            "NAME \n" +
            "\n" +
            "git - the stupid content tracker \n" +
            "SYNOPSIS \n" +
            "git [--version] [--help] [-C <path>] [-c <name>=<value>] [--exec-path[=<path>]] [--html-path] [--man-path] [-- \n" +
            "info-path]\n" +
            "    [-p|--paginate|-P|--no-pager] [--no-replace-objects]\n" +
            "[--bare]\n" +
            "    [--git-dir=<path>] [--work-tree=<path>] [--\n" +
            "namespace=<name>]\n" +
            "    [--super-prefix=<path>]\n" +
            "    <command> [<args>]\n" +
            "DESCRIPTION \n" +
            "Git is a fast, scalable, distributed revision control system with an unusually \n" +
            "rich command set that provides both high-level operations and full access to \n" +
            "internals. \n" +
            "See gittutorial[7] to get started, then see giteveryday[7] for a useful minimum \n" +
            "set of commands. The Git User’s Manual has a more in-depth introduction. \n" +
            "After you mastered the basic concepts, you can come back to this page to \n" +
            "learn what commands Git offers. You can learn more about individual Git \n" +
            "commands with \"git help command\". gitcli[7] manual page gives you an \n" +
            "overview of the command-line command syntax. \n" +
            "A formatted and hyperlinked copy of the latest Git documentation can be \n" +
            "viewed at \n" +
            "\n" +
            "https://git.github.io/htmldocs/git.html. OPTIONS \n" +
            "\n" +
            "--version \n" +
            "\n" +
            "Prints the Git suite version that the git program came from. \n" +
            "--help \n" +
            "Prints the synopsis and a list of the most commonly used commands. If the \n" +
            "option \n" +
            "--all \n" +
            "or \n" +
            "-a \n" +
            "is given then all available commands are printed. If a Git \n" +
            "command is named this option will bring up the manual page for that \n" +
            "command. \n" +
            "Other options are available to control how the manual page is displayed. See \n" +
            "git-help[1] for more information, because \n" +
            "is converted \n" +
            "internally into \n" +
            "-C <path> \n" +
            "git help .... \n" +
            "git --help ...\n" +
            " \n" +
            "Run as if git was started in <path> instead of the current working directory. \n" +
            "When multiple \n" +
            "-C \n" +
            "options are given, each subsequent non-absolute \n" +
            "is interpreted relative to the preceding \n" +
            "This option affects options that expect path name like \n" +
            "and \n" +
            "in that their interpretations of the path names would be made \n" +
            "relative to the working directory caused by the \n" +
            "option. For example the \n" +
            "following invocations are equivalent: \n" +
            "-C \n" +
            "git --git-dir=a.git --work-tree=b -C c status\n" +
            "git --git-dir=c/a.git --work-tree=c/b status\n" +
            "-C --git-dir -- \n" +
            "\n" +
            "<path> -C <path>. work-tree \n" +
            "-c <name>=<value> \n" +
            "Pass a configuration parameter to the command. The value given will override \n" +
            "values from configuration files. The <name> is expected in the same format \n" +
            "as listed by git config (subkeys separated by dots). \n" +
            "\n" +
            "Note that omitting the \n" +
            "= \n" +
            "in \n" +
            "is allowed and sets \n" +
            "to the boolean true value (just like \n" +
            "file). Including the equals but with an empty value (like \n" +
            "would in a config \n" +
            ") sets \n" +
            "to the empty string which \n" +
            "will convert to \n" +
            "foo.bar\u2028foo.bar= ... foo.bar \n" +
            "git -c\n" +
            "  git config --\n" +
            "git -c foo.bar ...\n" +
            "             [foo]bar\n" +
            "\n" +
            "type=bool \n" +
            "false. \n" +
            "--exec-path[=<path>] \n" +
            "Path to wherever your core Git programs are installed. This can also be \n" +
            "controlled by setting the GIT_EXEC_PATH environment variable. If no path \n" +
            "is given, git will print the current setting and then exit. \n" +
            "\n" +
            "--html-path \n" +
            "\n" +
            "Print the path, without trailing slash, where Git’s HTML documentation is \n" +
            "\n" +
            "installed and exit. \n" +
            "\n" +
            "--man-path \n" +
            "\n" +
            "Print the manpath (see \n" +
            "man(1)) for the man pages for this version of Git and \n" +
            "\n" +
            "exit. \n" +
            "\n" +
            "--info-path \n" +
            "\n" +
            "Print the path where the Info files documenting this version of Git are \n" +
            "\n" +
            "installed and exit. \n" +
            "-p \n" +
            "\n" +
            "--paginate \n" +
            "\n" +
            "Pipe all output into less (or if set, $PAGER) if standard output is a terminal. \n" +
            " \n" +
            "This overrides the \n" +
            "pager.<cmd> \n" +
            "configuration options (see the \n" +
            "\n" +
            "\"Configuration Mechanism\" section below). \n" +
            "-P \n" +
            "\n" +
            "--no-pager \n" +
            "\n" +
            "Do not pipe Git output into a pager. \n" +
            "\n" +
            "--git-dir=<path> \n" +
            "\n" +
            "Set the path to the repository. This can also be controlled by setting the \n" +
            "\n" +
            "GIT_DIR \n" +
            "environment variable. It can be an absolute path or relative path to \n" +
            "\n" +
            "current working directory. \n" +
            "--work-tree=<path> \n" +
            "Set the path to the working tree. It can be an absolute path or a path relative \n" +
            "to the current working directory. This can also be controlled by setting the \n" +
            "GIT_WORK_TREE environment variable and the core.worktree \n" +
            "configuration variable (see core.worktree in git-config[1] for a more detailed \n" +
            "discussion). \n" +
            "\n" +
            "--namespace=<path> \n" +
            "\n" +
            "Set the Git namespace. See gitnamespaces[7] for more details. Equivalent to \n" +
            " \n" +
            "setting the \n" +
            "--noglob-pathspecs \n" +
            "GIT_NAMESPACE\n" +
            "environment variable. \n" +
            "--super-prefix=<path> \n" +
            "Currently for internal use only. Set a prefix which gives a path from above a \n" +
            "repository down to its root. One use is to give submodules context about the \n" +
            "superproject that invoked it. \n" +
            "\n" +
            "--bare \n" +
            "\n" +
            "Treat the repository as a bare repository. If GIT_DIR environment is not set, \n" +
            "\n" +
            "it is set to the current working directory. \n" +
            "\n" +
            "--no-replace-objects \n" +
            "\n" +
            "Do not use replacement refs to replace Git objects. See git-replace[1] for more \n" +
            "\n" +
            "information. \n" +
            "\n" +
            "--literal-pathspecs \n" +
            "\n" +
            "Treat pathspecs literally (i.e. no globbing, no pathspec magic). This is \n" +
            " \n" +
            "equivalent to setting the \n" +
            "GIT_LITERAL_PATHSPECS\n" +
            "environment variable to \n" +
            " \n" +
            "1. \n" +
            "GIT_GLOB_PATHSPECS\n" +
            "--glob-pathspecs \n" +
            "Add \"glob\" magic to all pathspec. This is equivalent to setting the \n" +
            "\n" +
            "environment variable to \n" +
            "1 \n" +
            ". Disabling globbing on \n" +
            "individual pathspecs can be done using pathspec magic \":(literal)\" \n" +
            "\n" +
            "Add \"literal\" magic to all pathspec. This is equivalent to setting the \n" +
            "  \n" +
            "GIT_NOGLOB_PATHSPECS\n" +
            "GIT_ICASE_PATHSPECS 1. GIT_OPTIONAL_LOCKS 0. \n" +
            "--icase-pathspecs \n" +
            " \n" +
            "--no-optional-locks \n" +
            "setting the \n" +
            "environment variable to \n" +
            "\n" +
            "individual pathspecs can be done using pathspec magic \":(glob)\" \n" +
            " \n" +
            "Add \"icase\" magic to all pathspec. This is equivalent to setting the \n" +
            "\n" +
            "environment variable to \n" +
            " \n" +
            "Do not perform optional operations that require locks. This is equivalent to \n" +
            "   \n" +
            "to \n" +
            "--list-cmds=group[,group... ] \n" +
            "List commands by group. This is an internal/experimental option and may \n" +
            "change or be removed in the future. Supported groups are: builtins, parseopt \n" +
            "(builtin commands that use parse-options), main (all commands in libexec \n" +
            "directory), others (all other commands in \n" +
            "<category> (see categories in command-list.txt), nohelpers (exclude helper \n" +
            "$PATH \n" +
            "that have git- prefix), list- \n" +
            "commands), alias and config (retrieve command list from config variable \n" +
            "completion.commands) \n" +
            "\n" +
            "GIT COMMANDS \n" +
            "\n" +
            "We divide Git into high level (\"porcelain\") commands and low level \n" +
            "\n" +
            "(\"plumbing\") commands. \n" +
            "\n" +
            "High-level commands (porcelain) \n" +
            "\n" +
            "We separate the porcelain commands into the main commands and some \n" +
            "\n" +
            "ancillary user utilities. \n" +
            "Main porcelain commands \n" +
            "1 \n" +
            ". Enabling globbing on \n" +
            "\n" +
            "git-add[1] \n" +
            " \n" +
            "Add file contents to the index \n" +
            "\n" +
            "git-am[1] \n" +
            " \n" +
            "Apply a series of patches from a mailbox \n" +
            "\n" +
            "git-archive[1] \n" +
            " \n" +
            "Create an archive of files from a named tree \n" +
            "\n" +
            "git-bisect[1] \n" +
            " \n" +
            "Use binary search to find the commit that introduced a bug \n" +
            "\n" +
            "git-branch[1] \n" +
            " \n" +
            "List, create, or delete branches \n" +
            "\n" +
            "git-bundle[1] \n" +
            " \n" +
            "Move objects and refs by archive \n" +
            "\n" +
            "git-checkout[1] \n" +
            " \n" +
            "Switch branches or restore working tree files \n" +
            "\n" +
            "git-cherry-pick[1] \n" +
            " \n" +
            "Apply the changes introduced by some existing commits \n" +
            "\n" +
            "git-citool[1] \n" +
            " \n" +
            "Graphical alternative to git-commit \n" +
            "\n" +
            "git-clean[1] \n" +
            " \n" +
            "Remove untracked files from the working tree \n" +
            "\n" +
            "git-clone[1] \n" +
            " \n" +
            "Clone a repository into a new directory \n" +
            "\n" +
            "git-commit[1] \n" +
            " \n" +
            "Record changes to the repository \n" +
            "\n" +
            "git-describe[1] \n" +
            " \n" +
            "Give an object a human readable name based on an available ref \n" +
            "\n" +
            "git-diff[1] \n" +
            " \n" +
            "Show changes between commits, commit and working tree, etc \n" +
            "\n" +
            "git-fetch[1] \n" +
            " \n" +
            "Download objects and refs from another repository \n" +
            "\n" +
            "git-format-patch[1] \n" +
            " \n" +
            "Prepare patches for e-mail submission \n" +
            "\n" +
            "git-gc[1] \n" +
            " \n" +
            "Cleanup unnecessary files and optimize the local repository \n" +
            "\n" +
            "git-grep[1] \n" +
            " \n" +
            "Print lines matching a pattern \n" +
            "\n" +
            "git-gui[1] \n" +
            " \n" +
            "A portable graphical interface to Git \n" +
            "\n" +
            "git-init[1] \n" +
            " \n" +
            "Create an empty Git repository or reinitialize an existing one \n" +
            "\n" +
            "gitk[1] \n" +
            " \n" +
            "The Git repository browser \n" +
            "\n" +
            "git-log[1] \n" +
            " \n" +
            "Show commit logs \n" +
            "\n" +
            "git-merge[1] \n" +
            " \n" +
            "Join two or more development histories together \n" +
            "\n" +
            "git-mv[1] \n" +
            " \n" +
            "Move or rename a file, a directory, or a symlink \n" +
            "\n" +
            "git-notes[1] \n" +
            " \n" +
            "Add or inspect object notes \n" +
            "\n" +
            "git-pull[1] \n" +
            " \n" +
            "Fetch from and integrate with another repository or a local branch \n" +
            "\n" +
            "git-push[1] \n" +
            " \n" +
            "Update remote refs along with associated objects \n" +
            "\n" +
            "git-range-diff[1] \n" +
            " \n" +
            "Compare two commit ranges (e.g. two versions of a branch) \n" +
            "\n" +
            "git-rebase[1] \n" +
            " \n" +
            "Reapply commits on top of another base tip \n" +
            "\n" +
            "git-reset[1] \n" +
            " \n" +
            "Reset current HEAD to the specified state \n" +
            "\n" +
            "git-revert[1] \n" +
            " \n" +
            "Revert some existing commits \n" +
            "\n" +
            "git-rm[1] \n" +
            " \n" +
            "Remove files from the working tree and from the index \n" +
            "\n" +
            "git-shortlog[1] \n" +
            " \n" +
            "Summarize git log output \n" +
            "\n" +
            "git-show[1] \n" +
            " \n" +
            "Show various types of objects \n" +
            "\n" +
            "git-stash[1] \n" +
            " \n" +
            "Stash the changes in a dirty working directory away \n" +
            "\n" +
            "git-status[1] \n" +
            " \n" +
            "Show the working tree status \n" +
            "\n" +
            "git-submodule[1] \n" +
            " \n" +
            "Initialize, update or inspect submodules \n" +
            "\n" +
            "git-tag[1] \n" +
            " \n" +
            "Create, list, delete or verify a tag object signed with GPG \n" +
            "\n" +
            "git-worktree[1] \n" +
            " \n" +
            "Manage multiple working trees \n" +
            "\n" +
            "Ancillary Commands \n" +
            "\n" +
            "Manipulators: \n" +
            "\n" +
            "git-config[1] \n" +
            " \n" +
            "Get and set repository or global options \n" +
            "\n" +
            "git-fast-export[1] \n" +
            " \n" +
            "Git data exporter \n" +
            "\n" +
            "git-fast-import[1] \n" +
            " \n" +
            "Backend for fast Git data importers \n" +
            "\n" +
            "git-filter-branch[1] \n" +
            " \n" +
            "Rewrite branches \n" +
            "\n" +
            "git-mergetool[1] \n" +
            " \n" +
            "Run merge conflict resolution tools to resolve merge conflicts \n" +
            "\n" +
            "git-pack-refs[1] \n" +
            " \n" +
            "Pack heads and tags for efficient repository access \n" +
            "\n" +
            "git-prune[1] \n" +
            " \n" +
            "Prune all unreachable objects from the object database \n" +
            "\n" +
            "git-reflog[1] \n" +
            " \n" +
            "Manage reflog information \n" +
            "\n" +
            "git-remote[1] \n" +
            " \n" +
            "Manage set of tracked repositories \n" +
            "\n" +
            "git-repack[1] \n" +
            " \n" +
            "Pack unpacked objects in a repository \n" +
            "\n" +
            "git-replace[1] \n" +
            " \n" +
            "Create, list, delete refs to replace objects \n" +
            "\n" +
            "Interrogators: \n" +
            "\n" +
            "git-annotate[1] \n" +
            " \n" +
            "Annotate file lines with commit information \n" +
            "\n" +
            "git-blame[1] \n" +
            " \n" +
            "Show what revision and author last modified each line of a file \n" +
            "\n" +
            "git-count-objects[1] \n" +
            " \n" +
            "Count unpacked number of objects and their disk consumption \n" +
            "\n" +
            "git-difftool[1] \n" +
            " \n" +
            "Show changes using common diff tools \n" +
            "\n" +
            "git-fsck[1] \n" +
            " \n" +
            "Verifies the connectivity and validity of the objects in the database \n" +
            "\n" +
            "git-help[1] \n" +
            " \n" +
            "Display help information about Git \n" +
            "\n" +
            "git-instaweb[1] \n" +
            " \n" +
            "Instantly browse your working repository in gitweb \n" +
            "\n" +
            "git-merge-tree[1] \n" +
            " \n" +
            "Show three-way merge without touching index \n" +
            "\n" +
            "git-rerere[1] \n" +
            " \n" +
            "Reuse recorded resolution of conflicted merges \n" +
            "\n" +
            "git-show-branch[1] \n" +
            " \n" +
            "Show branches and their commits \n" +
            "\n" +
            "git-verify-commit[1] \n" +
            " \n" +
            "Check the GPG signature of commits \n" +
            "\n" +
            "git-verify-tag[1] \n" +
            " \n" +
            "Check the GPG signature of tags \n" +
            "\n" +
            "gitweb[1] \n" +
            " \n" +
            "Git web interface (web frontend to Git repositories) \n" +
            "\n" +
            "git-whatchanged[1] \n" +
            " \n" +
            "Show logs with difference each commit introduces \n" +
            "\n" +
            "Interacting with Others \n" +
            "\n" +
            "These commands are to interact with foreign SCM and with other people via \n" +
            "\n" +
            "patch over e-mail. \n" +
            "\n" +
            "git-archimport[1] \n" +
            " \n" +
            "Import a GNU Arch repository into Git \n" +
            "\n" +
            "git-cvsexportcommit[1] \n" +
            " \n" +
            "Export a single commit to a CVS checkout \n" +
            "\n" +
            "git-cvsimport[1] \n" +
            " \n" +
            "Salvage your data out of another SCM people love to hate \n" +
            "\n" +
            "git-cvsserver[1] \n" +
            " \n" +
            "A CVS server emulator for Git \n" +
            "\n" +
            "git-imap-send[1] \n" +
            " \n" +
            "Send a collection of patches from stdin to an IMAP folder \n" +
            "\n" +
            "git-p4[1] \n" +
            " \n" +
            "Import from and submit to Perforce repositories \n" +
            "\n" +
            "git-quiltimport[1] \n" +
            " \n" +
            "Applies a quilt patchset onto the current branch \n" +
            "\n" +
            "git-request-pull[1] \n" +
            " \n" +
            "Generates a summary of pending changes \n" +
            "\n" +
            "git-send-email[1] \n" +
            " \n" +
            "Send a collection of patches as emails \n" +
            "\n" +
            "git-svn[1] \n" +
            " \n" +
            "Bidirectional operation between a Subversion repository and Git \n" +
            "Low-level commands (plumbing) \n" +
            "Although Git includes its own porcelain layer, its low-level commands are \n" +
            "sufficient to support development of alternative porcelains. Developers of \n" +
            "such porcelains might start by reading about git-update-index[1] and git- \n" +
            "read-tree[1]. \n" +
            "The interface (input, output, set of options and the semantics) to these low- \n" +
            "level commands are meant to be a lot more stable than Porcelain level \n" +
            "commands, because these commands are primarily for scripted use. The \n" +
            "interface to Porcelain commands on the other hand are subject to change in \n" +
            "order to improve the end user experience. \n" +
            "The following description divides the low-level commands into commands \n" +
            "that manipulate objects (in the repository, index, and working tree), \n" +
            "commands that interrogate and compare objects, and commands that move \n" +
            "objects and references between repositories. \n" +
            "Manipulation commands \n" +
            "\n" +
            "git-apply[1] \n" +
            " \n" +
            "Apply a patch to files and/or to the index \n" +
            "\n" +
            "git-checkout-index[1] \n" +
            " \n" +
            "Copy files from the index to the working tree \n" +
            "\n" +
            "git-commit-graph[1] \n" +
            " \n" +
            "Write and verify Git commit-graph files \n" +
            "\n" +
            "git-commit-tree[1] \n" +
            " \n" +
            "Create a new commit object \n" +
            "\n" +
            "git-hash-object[1] \n" +
            " \n" +
            "Compute object ID and optionally creates a blob from a file \n" +
            "\n" +
            "git-index-pack[1] \n" +
            " \n" +
            "Build pack index file for an existing packed archive \n" +
            "\n" +
            "git-merge-file[1] \n" +
            " \n" +
            "Run a three-way file merge \n" +
            "\n" +
            "git-merge-index[1] \n" +
            " \n" +
            "Run a merge for files needing merging \n" +
            "\n" +
            "git-multi-pack-index[1] \n" +
            " \n" +
            "Write and verify multi-pack-indexes \n" +
            "\n" +
            "git-mktag[1] \n" +
            " \n" +
            "Creates a tag object \n" +
            "\n" +
            "git-mktree[1] \n" +
            " \n" +
            "Build a tree-object from ls-tree formatted text \n" +
            "\n" +
            "git-pack-objects[1] \n" +
            " \n" +
            "Create a packed archive of objects \n" +
            "\n" +
            "git-prune-packed[1] \n" +
            " \n" +
            "Remove extra objects that are already in pack files \n" +
            "\n" +
            "git-read-tree[1] \n" +
            " \n" +
            "Reads tree information into the index \n" +
            "\n" +
            "git-symbolic-ref[1] \n" +
            " \n" +
            "Read, modify and delete symbolic refs \n" +
            "\n" +
            "git-unpack-objects[1] \n" +
            " \n" +
            "Unpack objects from a packed archive \n" +
            "\n" +
            "git-update-index[1] \n" +
            " \n" +
            "Register file contents in the working tree to the index \n" +
            "\n" +
            "git-update-ref[1] \n" +
            " \n" +
            "Update the object name stored in a ref safely \n" +
            "\n" +
            "git-write-tree[1] \n" +
            " \n" +
            "Create a tree object from the current index \n" +
            "Interrogation commands \n" +
            "\n" +
            "git-cat-file[1] \n" +
            " \n" +
            "Provide content or type and size information for repository objects \n" +
            "\n" +
            "git-cherry[1] \n" +
            " \n" +
            "Find commits yet to be applied to upstream \n" +
            "\n" +
            "git-diff-files[1] \n" +
            " \n" +
            "Compares files in the working tree and the index \n" +
            "\n" +
            "git-diff-index[1] \n" +
            " \n" +
            "Compare a tree to the working tree or index \n" +
            "\n" +
            "git-diff-tree[1] \n" +
            " \n" +
            "Compares the content and mode of blobs found via two tree objects \n" +
            "\n" +
            "git-for-each-ref[1] \n" +
            " \n" +
            "Output information on each ref \n" +
            "git-get-tar-commit-id[1] \n" +
            "Extract commit ID from an archive created using git-archive \n" +
            "\n" +
            "git-ls-files[1] \n" +
            " \n" +
            "Show information about files in the index and the working tree \n" +
            "\n" +
            "git-ls-remote[1] \n" +
            " \n" +
            "List references in a remote repository \n" +
            "\n" +
            "git-ls-tree[1] \n" +
            " \n" +
            "List the contents of a tree object \n" +
            "\n" +
            "git-merge-base[1] \n" +
            " \n" +
            "Find as good common ancestors as possible for a merge \n" +
            "\n" +
            "git-name-rev[1] \n" +
            " \n" +
            "Find symbolic names for given revs \n" +
            "\n" +
            "\n" +
            "\n" +
            "git-rev-list[1] \n" +
            " \n" +
            "Lists commit objects in reverse chronological order \n" +
            "\n" +
            "git-rev-parse[1] \n" +
            " \n" +
            "Pick out and massage parameters \n" +
            "\n" +
            "git-show-index[1] \n" +
            " \n" +
            "Show packed archive index \n" +
            "\n" +
            "git-show-ref[1] \n" +
            " \n" +
            "List references in a local repository \n" +
            "\n" +
            "git-unpack-file[1] \n" +
            " \n" +
            "Creates a temporary file with a blob’s contents \n" +
            "\n" +
            "git-var[1] \n" +
            " \n" +
            "Show a Git logical variable \n" +
            "\n" +
            "git-verify-pack[1] \n" +
            " \n" +
            "Validate packed Git archive files \n" +
            "\n" +
            "In general, the interrogate commands do not touch the files in the working \n" +
            "\n" +
            "tree. \n" +
            "Synching repositories \n" +
            "\n" +
            "git-daemon[1] \n" +
            " \n" +
            "A really simple server for Git repositories \n" +
            "\n" +
            "git-fetch-pack[1] \n" +
            " \n" +
            "Receive missing objects from another repository \n" +
            "\n" +
            "git-http-backend[1] \n" +
            " \n" +
            "Server side implementation of Git over HTTP \n" +
            "\n" +
            "git-send-pack[1] \n" +
            " \n" +
            "Push objects over Git protocol to another repository \n" +
            "\n" +
            "git-update-server-info[1] \n" +
            " \n" +
            "Update auxiliary info file to help dumb servers \n" +
            "\n" +
            "The following are helper commands used by the above; end users typically do \n" +
            "\n" +
            "not use them directly. \n" +
            "\n" +
            "git-http-fetch[1] \n" +
            " \n" +
            "Download from a remote Git repository via HTTP \n" +
            "\n" +
            "git-http-push[1] \n" +
            " \n" +
            "Push objects over HTTP/DAV to another repository \n" +
            "\n" +
            "git-parse-remote[1] \n" +
            " \n" +
            "Routines to help parsing remote repository access parameters \n" +
            "\n" +
            "git-receive-pack[1] \n" +
            " \n" +
            "Receive what is pushed into the repository \n" +
            "\n" +
            "git-shell[1] \n" +
            " \n" +
            "Restricted login shell for Git-only SSH access \n" +
            "\n" +
            "git-upload-archive[1] \n" +
            " \n" +
            "Send archive back to git-archive \n" +
            "\n" +
            "git-upload-pack[1] \n" +
            " \n" +
            "Send objects packed back to git-fetch-pack \n" +
            "\n" +
            "Internal helper commands \n" +
            "\n" +
            "These are internal helper commands used by other commands; end users \n" +
            "\n" +
            "typically do not use them directly. \n" +
            "\n" +
            "git-check-attr[1] \n" +
            " \n" +
            "Display gitattributes information \n" +
            "\n" +
            "git-check-ignore[1] \n" +
            " \n" +
            "Debug gitignore / exclude files \n" +
            "\n" +
            "git-check-mailmap[1] \n" +
            " \n" +
            "Show canonical names and email addresses of contacts \n" +
            "\n" +
            "git-check-ref-format[1] \n" +
            " \n" +
            "Ensures that a reference name is well formed \n" +
            "\n" +
            "git-column[1] \n" +
            " \n" +
            "Display data in columns \n" +
            "\n" +
            "git-credential[1] \n" +
            " \n" +
            "Retrieve and store user credentials \n" +
            "\n" +
            "git-credential-cache[1] \n" +
            " \n" +
            "Helper to temporarily store passwords in memory \n" +
            "\n" +
            "git-credential-store[1] \n" +
            " \n" +
            "Helper to store credentials on disk \n" +
            "\n" +
            "git-fmt-merge-msg[1] \n" +
            " \n" +
            "Produce a merge commit message \n" +
            "\n" +
            "git-interpret-trailers[1] \n" +
            " \n" +
            "add or parse structured information in commit messages \n" +
            "\n" +
            "git-mailinfo[1] \n" +
            " \n" +
            "Extracts patch and authorship from a single e-mail message \n" +
            "\n" +
            "git-mailsplit[1] \n" +
            " \n" +
            "Simple UNIX mbox splitter program \n" +
            "\n" +
            "git-merge-one-file[1] \n" +
            " \n" +
            "The standard helper program to use with git-merge-index \n" +
            "\n" +
            "git-patch-id[1] \n" +
            " \n" +
            "Compute unique ID for a patch \n" +
            "\n" +
            "git-sh-i18n[1] \n" +
            " \n" +
            "Git’s i18n setup code for shell scripts \n" +
            "\n" +
            "git-sh-setup[1] \n" +
            " \n" +
            "Common Git shell script setup code \n" +
            "\n" +
            "git-stripspace[1] \n" +
            " \n" +
            "Remove unnecessary whitespace \n" +
            "\n" +
            "Configuration Mechanism \n" +
            "\n" +
            "Git uses a simple text format to store customizations that are per repository \n" +
            "\n" +
            "and are per user. Such a configuration file may look like this: \n" +
            "#\n" +
            "# A '#' or ';' character indicates a comment.\n" +
            "#\n" +
            "; core variables\n" +
            "[core]\n" +
            "    ; Don't trust file modes\n" +
            "    filemode = false\n" +
            "; user identity\n" +
            "[user]\n" +
            "    name = \"Junio C Hamano\"\n" +
            "    email = \"gitster@pobox.com\"\n" +
            "Identifier Terminology \n" +
            "\n" +
            "Various commands read from the configuration file and adjust their operation \n" +
            "\n" +
            "accordingly. See git-config[1] for a list and more details about the \n" +
            "\n" +
            "configuration mechanism. \n" +
            "\n" +
            "<object> \n" +
            "\n" +
            "Indicates the object name for any type of object. \n" +
            "\n" +
            "<blob> \n" +
            "\n" +
            "Indicates a blob object name. \n" +
            "\n" +
            "<tree> \n" +
            "\n" +
            "Indicates a tree object name. \n" +
            "\n" +
            "<commit> \n" +
            "\n" +
            "Indicates a commit object name. \n" +
            "<tree-ish> \n" +
            "Indicates a tree, commit or tag object name. A command that takes a <tree- \n" +
            "ish> argument ultimately wants to operate on a <tree> object but \n" +
            "automatically dereferences <commit> and <tag> objects that point at a \n" +
            "<tree>. \n" +
            "<commit-ish> \n" +
            "Indicates a commit or tag object name. A command that takes a <commit- \n" +
            "ish> argument ultimately wants to operate on a <commit> object but \n" +
            "automatically dereferences <tag> objects that point at a <commit>. \n" +
            "\n" +
            "<type> \n" +
            "  \n" +
            "Indicates that an object type is required. Currently one of: \n" +
            "Any Git command accepting any <object> can also use the following symbolic \n" +
            "blob, tree, \n" +
            " \n" +
            "commit tag. GIT_INDEX_FILE \n" +
            "<file> \n" +
            ", or \n" +
            " \n" +
            "Indicates a filename - almost always relative to the root of the tree structure \n" +
            "\n" +
            "Symbolic Identifiers \n" +
            "describes. \n" +
            "  \n" +
            "notation: \n" +
            "\n" +
            "HEAD \n" +
            "\n" +
            "indicates the head of the current branch. \n" +
            "\n" +
            "<tag> \n" +
            "\n" +
            "a valid tag name (i.e. a \n" +
            "refs/tags/<tag> reference). refs/heads/<head> \n" +
            "\n" +
            "<head> \n" +
            " \n" +
            "a valid head name (i.e. a \n" +
            "reference). \n" +
            "\n" +
            "For a more complete list of ways to spell object names, see \"SPECIFYING \n" +
            "\n" +
            "REVISIONS\" section in gitrevisions[7]. \n" +
            "File/Directory Structure \n" +
            "Please see the gitrepository-layout[5] document. \n" +
            "Read githooks[5] for more details about each hook. \n" +
            "Higher level SCMs may provide and manage additional information in the \n" +
            "\n" +
            "$GIT_DIR. \n" +
            "\n" +
            "Terminology \n" +
            "\n" +
            "Please see gitglossary[7]. \n" +
            " \n" +
            "Environment Variables \n" +
            "\n" +
            "Various Git commands use the following environment variables: \n" +
            "The Git Repository \n" +
            "\n" +
            "These environment variables apply to all core Git commands. Nb: it is worth \n" +
            "\n" +
            "noting that they may be used/overridden by SCMS sitting above Git so take \n" +
            "\n" +
            "care if using a foreign front-end. \n" +
            "GIT_INDEX_FILE\n" +
            "GIT_INDEX_VERSION\n" +
            "$GIT_DIR/index\n" +
            "\n" +
            "This environment allows the specification of an alternate index file. If not \n" +
            " \n" +
            "specified, the default of \n" +
            "If the object storage directory is specified via this environment variable then \n" +
            "is used. \n" +
            "This environment variable allows the specification of an index version for new \n" +
            "repositories. It won’t affect existing index files. By default index file version 2 \n" +
            "or 3 is used. See git-update-index[1] for more information. \n" +
            "GIT_OBJECT_DIRECTORY\n" +
            "$GIT_DIR/objects\n" +
            "GIT_ALTERNATE_OBJECT_DIRECTORIES\n" +
            " \n" +
            "the sha1 directories are created underneath - otherwise the default \n" +
            "\n" +
            "directory is used. \n" +
            "Due to the immutable nature of Git objects, old objects can be archived into \n" +
            "shared, read-only directories. This variable specifies a \":\" separated (on \n" +
            "Windows \";\" separated) list of Git object directories which can be used to \n" +
            "search for Git objects. New objects will not be written to these directories. \n" +
            "\n" +
            "Entries that begin with \n" +
            "\" \n" +
            "(double-quote) will be interpreted as C-style quoted \n" +
            "paths, removing leading and trailing double-quotes and respecting backslash \n" +
            "escapes. E.g., the value \n" +
            "has two paths: \n" +
            "\"path-with-\\\"-and-:-in-it\":vanilla-path path-with-\"-and-:-in-it and vanilla-path. \n" +
            "\n" +
            "GIT_DIR \n" +
            "GIT_DIR \n" +
            "GIT_WORK_TREE\n" +
            "--work-tree \n" +
            "variable. \n" +
            "GIT_NAMESPACE\n" +
            "GIT_CEILING_DIRECTORIES\n" +
            "--git-dir \n" +
            " \n" +
            "If the \n" +
            "environment variable is set then it specifies a path to use \n" +
            " \n" +
            "instead of the default \n" +
            "command-line option also sets this value. \n" +
            ".git \n" +
            "for the base of the repository. The \n" +
            " \n" +
            "Set the path to the root of the working tree. This can also be controlled by the \n" +
            "\n" +
            "command-line option and the core.worktree configuration \n" +
            "\n" +
            "Set the Git namespace; see gitnamespaces[7] for details. The \n" +
            "--namespace \n" +
            "\n" +
            "command-line option also sets this value. \n" +
            "This should be a colon-separated list of absolute paths. If set, it is a list of \n" +
            "directories that Git should not chdir up into while looking for a repository \n" +
            "directory (useful for excluding slow-loading network directories). It will not \n" +
            "exclude the current working directory or a GIT_DIR set on the command line \n" +
            "or in the environment. Normally, Git has to read the entries in this list and \n" +
            "resolve any symlink that might be present in order to compare them with the \n" +
            "current directory. However, if even this access is slow, you can add an empty \n" +
            "entry to the list to tell Git that the subsequent entries are not symlinks and \n" +
            "needn’t be resolved; e.g., \n" +
            "GIT_CEILING_DIRECTORIES=/maybe/ symlink::/very/slow/non/symlink. \n" +
            "GIT_DISCOVERY_ACROSS_FILESYSTEM\n" +
            "             GIT_CEILING_DIRECTORIES\n" +
            "                          GIT_DIR\n" +
            "GIT_COMMON_DIR\n" +
            "Git Commits \n" +
            "GIT_AUTHOR_NAME\n" +
            "GIT_AUTHOR_EMAIL\n" +
            "GIT_AUTHOR_DATE\n" +
            "GIT_COMMITTER_NAME\n" +
            "GIT_COMMITTER_EMAIL\n" +
            "GIT_COMMITTER_DATE\n" +
            "\n" +
            "When run in a directory that does not have \".git\" repository directory, Git \n" +
            "tries to find such a directory in the parent directories to find the top of the \n" +
            "working tree, but by default it does not cross filesystem boundaries. This \n" +
            "environment variable can be set to true to tell Git not to stop at filesystem \n" +
            "boundaries. Like \n" +
            ", this will not affect an \n" +
            "explicit repository directory set via \n" +
            "or on the command line. \n" +
            "If this variable is set to a path, non-worktree files that are normally in \n" +
            "$GIT_DIR will be taken from this path instead. Worktree-specific files such \n" +
            "as HEAD or index are taken from $GIT_DIR. See gitrepository-layout[5] and \n" +
            "git-worktree[1] for details. This variable has lower precedence than other path \n" +
            "variables such as GIT_INDEX_FILE, GIT_OBJECT_DIRECTORY... \n" +
            "\n" +
            "EMAIL \n" +
            "\n" +
            "see git-commit-tree[1] \n" +
            "\n" +
            "Git Diffs \n" +
            "GIT_DIFF_OPTS\n" +
            "GIT_EXTERNAL_DIFF\n" +
            "Only valid setting is \"--unified=??\" or \"-u??\" to set the number of context lines \n" +
            "shown when a unified diff is created. This takes precedence over any \"-U\" or \n" +
            "\"--unified\" option value passed on the Git diff command line. \n" +
            " \n" +
            "When the environment variable \n" +
            "GIT_EXTERNAL_DIFF\n" +
            "       GIT_EXTERNAL_DIFF\n" +
            "named by it is called, instead of the diff invocation described above. For a \n" +
            " \n" +
            "path that is added, removed, or modified, \n" +
            "path old-file old-hex old-mode new-file new-hex new-mode\n" +
            "is called \n" +
            "\n" +
            "with 7 parameters: \n" +
            "where: \n" +
            "\n" +
            "<old|new>-file \n" +
            "\n" +
            "are files GIT_EXTERNAL_DIFF can use to read the contents of <old|new>, \n" +
            "\n" +
            "<old|new>-hex \n" +
            "\n" +
            "are the 40-hexdigit SHA-1 hashes, \n" +
            "<old|new>-mode \n" +
            "are the octal representation of the file modes. \n" +
            "The file parameters can point at the user’s working file (e.g. \n" +
            "in \n" +
            "\"git-diff-files\"), \n" +
            "/dev/null\n" +
            "   old-file\n" +
            "(e.g. \n" +
            "old-file \n" +
            "when a new file is added), or a \n" +
            "temporary file (e.g. \n" +
            "in the index). \n" +
            "GIT_EXTERNAL_DIFF\n" +
            "should \n" +
            "not worry about unlinking the temporary file --- it is removed when \n" +
            "For a path that is unmerged, \n" +
            "exits. \n" +
            "parameter, <path>. \n" +
            "For each path \n" +
            "GIT_EXTERNAL_DIFF\n" +
            "          GIT_EXTERNAL_DIFF\n" +
            "GIT_EXTERNAL_DIFF is called with 1 GIT_DIFF_PATH_COUNTER and GIT_DIFF_PATH_TOTAL \n" +
            "is set, the program \n" +
            "\n" +
            "new-file \n" +
            "\n" +
            "is called, two environment variables, \n" +
            "\n" +
            "GIT_DIFF_PATH_COUNTER\n" +
            "A 1-based counter incremented by one for every path. \n" +
            "GIT_DIFF_PATH_TOTAL\n" +
            "The total number of paths. \n" +
            "other \n" +
            "GIT_MERGE_VERBOSITY\n" +
            "A number controlling the amount of output shown by the recursive merge \n" +
            "are set. \n" +
            " \n" +
            "strategy. Overrides merge.verbosity. See git-merge[1] \n" +
            "GIT_PAGER \n" +
            " \n" +
            "This environment variable overrides \n" +
            "GIT_EDITOR \n" +
            "GIT_SSH\n" +
            "GIT_SSH_COMMAND\n" +
            "$GIT_SSH_COMMAND\n" +
            ".ssh/config \n" +
            "GIT_SSH_VARIANT\n" +
            "GIT_SSH/GIT_SSH_COMMAND/core.sshCommand \n" +
            "$PAGER \n" +
            ". If it is set to an empty string or \n" +
            "\n" +
            "to the value \"cat\", Git will not launch a pager. See also the \n" +
            "\n" +
            "option in git-config[1]. \n" +
            "  \n" +
            "This environment variable overrides \n" +
            "$EDITOR $VISUAL core.editor \n" +
            "                 ssh.variant\n" +
            "             $GIT_SSH\n" +
            "and \n" +
            "core.pager \n" +
            ". It is used by \n" +
            "\n" +
            "several Git commands when, on interactive mode, an editor is to be launched. \n" +
            " \n" +
            "See also git-var[1] and the \n" +
            "option in git-config[1]. \n" +
            "If either of these environment variables is set then git fetch and git push will \n" +
            "use the specified command instead of ssh when they need to connect to a \n" +
            "remote system. The command-line parameters passed to the configured \n" +
            "command are determined by the ssh variant. See \n" +
            "option in git- \n" +
            "config[1] for details. \n" +
            "takes precedence over \n" +
            ", and is interpreted by \n" +
            "the shell, which allows additional arguments to be included. \n" +
            "$GIT_SSH \n" +
            "on \n" +
            "the other hand must be just the path to a program (which can be a wrapper \n" +
            "shell script, if additional arguments are needed). \n" +
            "Usually it is easier to configure any desired options through your \n" +
            "personal \n" +
            "file. Please consult your ssh documentation for \n" +
            "further details. \n" +
            "\n" +
            "If this environment variable is set, it overrides Git’s autodetection whether \n" +
            "  \n" +
            "serves the same purpose. \n" +
            "GIT_ASKPASS \n" +
            "GIT_TERMINAL_PROMPT\n" +
            "   core.askPass\n" +
            "0 \n" +
            "refer to OpenSSH, plink \n" +
            " \n" +
            "or tortoiseplink. This variable overrides the config setting \n" +
            "ssh.variant \n" +
            "that \n" +
            "\n" +
            "If this environment variable is set, then Git commands which need to acquire \n" +
            "passwords or passphrases (e.g. for HTTP or IMAP authentication) will call \n" +
            "this program with a suitable prompt as command-line argument and read the \n" +
            "password from its STDOUT. See also the \n" +
            "option in git- \n" +
            "config[1]. \n" +
            "  \n" +
            "If this environment variable is set to \n" +
            "GIT_CONFIG_NOSYSTEM\n" +
            ", git will not prompt on the terminal \n" +
            "\n" +
            "(e.g., when asking for HTTP authentication). \n" +
            "Whether to skip reading settings from the system-wide \n" +
            "file. This environment variable can be used along with \n" +
            "and \n" +
            "to create a predictable environment for a picky \n" +
            "script, or you can set it temporarily to avoid using a buggy \n" +
            "file while waiting for someone with sufficient permissions to fix it. \n" +
            "gitconfig\n" +
            "   $XDG_CONFIG_HOME\n" +
            "GIT_FLUSH \n" +
            "$(prefix)/etc/\n" +
            "         $HOME\n" +
            "  /etc/gitconfig\n" +
            "If this environment variable is set to \"1\", then commands such as git blame \n" +
            "(in incremental mode), git rev-list, git log, git check-attr and git check-ignore \n" +
            "will force a flush of the output stream after each record have been flushed. If \n" +
            "this variable is set to \"0\", the output of these commands will be done using \n" +
            "completely buffered I/O. If this environment variable is not set, Git will \n" +
            "choose buffered or record-oriented flushing based on whether stdout appears \n" +
            "to be redirected to a file or not. \n" +
            "GIT_TRACE \n" +
            "Enables general trace messages, e.g. alias expansion, built-in command \n" +
            "execution and external command execution. \n" +
            "If this variable is set to \"1\", \"2\" or \"true\" (comparison is case insensitive), trace \n" +
            "messages will be printed to stderr. \n" +
            "If the variable is set to an integer value greater than 2 and lower than 10 \n" +
            "(strictly) then Git will interpret this value as an open file descriptor and will \n" +
            "try to write the trace messages into this file descriptor. \n" +
            "Alternatively, if the variable is set to an absolute path (starting with a / \n" +
            "character), Git will interpret this as a file path and will try to append the trace \n" +
            "messages to it. \n" +
            "Unsetting the variable, or setting it to empty, \"0\" or \"false\" (case insensitive) \n" +
            "disables trace messages. \n" +
            "GIT_TRACE_FSMONITOR\n" +
            "GIT_TRACE_PACK_ACCESS\n" +
            "GIT_TRACE_PACKET\n" +
            "GIT_TRACE \n" +
            "GIT_TRACE \n" +
            "\n" +
            "Enables trace messages for the filesystem monitor extension. See \n" +
            "\n" +
            "for available trace output options. \n" +
            "Enables trace messages for all accesses to any packs. For each access, the pack \n" +
            "file name and an offset in the pack is recorded. This may be helpful for \n" +
            "troubleshooting some pack-related performance problems. See \n" +
            "for available trace output options. \n" +
            "Enables trace messages for all packets coming in or out of a given program. \n" +
            "This can help with debugging object negotiation or other protocol issues. \n" +
            "Tracing is turned off at a packet starting with \"PACK\" (but see \n" +
            "GIT_TRACE_PACKFILE below). See GIT_TRACE for available trace output options. \n" +
            "GIT_TRACE_PACKFILE\n" +
            "GIT_TRACE_PACKFILE=/tmp/my.pack\n" +
            "Enables tracing of packfiles sent or received by a given program. Unlike other \n" +
            "trace output, this trace is verbatim: no headers, and no quoting of binary data. \n" +
            "You almost certainly want to direct into a file (e.g., \n" +
            ") rather than displaying it on the \n" +
            "terminal or mixing it with other trace output. \n" +
            "Note that this is currently only implemented for the client side of clones and \n" +
            "fetches. \n" +
            "GIT_TRACE_PERFORMANCE\n" +
            "GIT_TRACE \n" +
            "GIT_TRACE_SETUP\n" +
            "GIT_TRACE_SHALLOW\n" +
            "GIT_TRACE \n" +
            "GIT_TRACE_CURL\n" +
            "--trace-ascii GIT_CURL_VERBOSE trace output options. \n" +
            "GIT_TRACE_CURL_NO_DATA\n" +
            "GIT_REDACT_COOKIES\n" +
            "GIT_LITERAL_PATHSPECS\n" +
            "GIT_TRACE \n" +
            "\n" +
            "Enables performance related trace messages, e.g. total execution time of each \n" +
            " \n" +
            "Git command. See \n" +
            "for available trace output options. \n" +
            "\n" +
            "Enables trace messages printing the .git, working tree and current working \n" +
            " \n" +
            "directory after Git has completed its setup phase. See \n" +
            "for \n" +
            "\n" +
            "available trace output options. \n" +
            "\n" +
            "Enables trace messages that can help debugging fetching / cloning of shallow \n" +
            " \n" +
            "repositories. See \n" +
            "Enables a curl full trace dump of all incoming and outgoing data, including \n" +
            "descriptive information, of the git transport protocol. This is similar to doing \n" +
            "curl \n" +
            "on the command line. This option overrides setting the \n" +
            "environment variable. See \n" +
            "for available \n" +
            "When a curl trace is enabled (see \n" +
            "for available trace output options. \n" +
            "GIT_TRACE \n" +
            "GIT_TRACE_CURL above), do not dump \n" +
            " \n" +
            "data (that is, only dump info lines and headers). \n" +
            "This can be set to a comma-separated list of strings. When a curl trace is \n" +
            "enabled (see \n" +
            "GIT_TRACE_CURL\n" +
            "above), whenever a \"Cookies:\" header sent by \n" +
            "the client is dumped, values of cookies whose key is in that list (case- \n" +
            "sensitive) are redacted. \n" +
            "Setting this variable to \n" +
            "will cause Git to treat all pathspecs literally, rather \n" +
            "than as glob patterns. For example, running \n" +
            "will search for commits that touch the path \n" +
            ", not \n" +
            "any paths that the glob \n" +
            "*.c \n" +
            "matches. You might want this if you are feeding \n" +
            "literal paths to Git (e.g., paths previously given to you by \n" +
            "diff output, etc). \n" +
            "\n" +
            "1\u2028git log -- '*.c' *.c \n" +
            "GIT_LITERAL_PATHSPECS=1 git ls-tree, -- \n" +
            "\n" +
            "raw \n" +
            "GIT_GLOB_PATHSPECS\n" +
            "1 will cause Git to treat all pathspecs as glob patterns GIT_NOGLOB_PATHSPECS \n" +
            "1 will cause Git to treat all pathspecs as literal (aka GIT_ICASE_PATHSPECS \n" +
            "1 will cause Git to treat all pathspecs as case- GIT_REFLOG_ACTION \n" +
            " \n" +
            "Setting this variable to \n" +
            "\n" +
            "(aka \"glob\" magic). \n" +
            " \n" +
            "Setting this variable to \n" +
            "\n" +
            "\"literal\" magic). \n" +
            " \n" +
            "Setting this variable to \n" +
            "\n" +
            "insensitive. \n" +
            "When a ref is updated, reflog entries are created to keep track of the reason \n" +
            "why the ref was updated (which is typically the name of the high-level \n" +
            "command that updated the ref), in addition to the old and new values of the \n" +
            "ref. A scripted Porcelain command can use set_reflog_action helper function \n" +
            "in \n" +
            "git-sh-setup\n" +
            "to set its name to this variable when it is invoked as the \n" +
            "top level command by the end user, to be recorded in the body of the reflog. \n" +
            "GIT_REF_PARANOIA\n" +
            "1 \n" +
            "GIT_ALLOW_PROTOCOL\n" +
            "    never\n" +
            "protocol.<name>.allow\n" +
            "protocol.allow\n" +
            "protocol.allow is \n" +
            "If set to \n" +
            ", include broken or badly named refs when iterating over lists of \n" +
            "refs. In a normal, non-corrupted repository, this does nothing. However, \n" +
            "enabling it may help git to detect and abort some operations in the presence \n" +
            "of broken refs. Git sets this variable automatically when performing \n" +
            "destructive operations like git-prune[1]. You should not need to set it yourself \n" +
            "unless you want to be paranoid about making sure an operation has touched \n" +
            "every ref (e.g., because you are cloning a repository to make a backup). \n" +
            "\n" +
            "If set to a colon-separated list of protocols, behave as if \n" +
            "set to \n" +
            ", and each of the listed protocols has \n" +
            "set to \n" +
            "always \n" +
            "configuration). In other words, any protocol not mentioned will be disallowed \n" +
            "(overriding any existing \n" +
            "(i.e., this is a whitelist, not a blacklist). See the description of \n" +
            "in git-config[1] for more details. \n" +
            "GIT_PROTOCOL_FROM_USER\n" +
            "Set to 0 to prevent protocols used by fetch/push/clone which are configured \n" +
            "to the \n" +
            "user \n" +
            "state. This is useful to restrict recursive submodule initialization \n" +
            "from an untrusted repository or for programs which feed potentially- \n" +
            "untrusted URLS to git commands. See git-config[1] for more details. \n" +
            "GIT_PROTOCOL\n" +
            "GIT_OPTIONAL_LOCKS\n" +
            "0 \n" +
            "GIT_REDIRECT_STDIN\n" +
            "GIT_REDIRECT_STDOUT\n" +
            "GIT_REDIRECT_STDERR\n" +
            "\\pipe\\my-git-stdin-123\n" +
            "                         off\n" +
            "1. \n" +
            "\n" +
            "For internal use only. Used in handshaking the wire protocol. Contains a \n" +
            "\n" +
            "colon : separated list of keys with optional values key[=value]. Presence of \n" +
            "\n" +
            "unknown keys and values must be ignored. \n" +
            "If set to \n" +
            ", Git will complete any requested operation without performing any \n" +
            "optional sub-operations that require taking a lock. For example, this will \n" +
            "prevent \n" +
            "git status \n" +
            "from refreshing the index as a side effect. This is useful \n" +
            "for processes running in the background which do not want to cause lock \n" +
            "contention with other operations on the repository. Defaults to \n" +
            "  \n" +
            "Windows-only: allow redirecting the standard input/output/error handles to \n" +
            "paths specified by the environment variables. This is particularly useful in \n" +
            "multi-threaded applications where the canonical way to pass standard \n" +
            "handles via \n" +
            "CreateProcess()\n" +
            "is not an option because it would require the \n" +
            "handles to be marked inheritable (and consequently every spawned process \n" +
            "would inherit them, possibly blocking regular Git operations). The primary \n" +
            "intended use case is to use named pipes for communication (e.g. \n" +
            "). \n" +
            "Two special values are supported: \n" +
            "will simply close the corresponding \n" +
            "standard handle, and if \n" +
            "is \n" +
            ", standard error will \n" +
            "be redirected to the same handle as standard output. \n" +
            "                 GIT_REDIRECT_STDERR  2>&1\n" +
            "GIT_PRINT_SHA1_ELLIPSIS\n" +
            "yes \n" +
            "Discussion \n" +
            "\\\\. \n" +
            "(deprecated) \n" +
            "If set to \n" +
            ", print an ellipsis following an (abbreviated) SHA-1 value. This \n" +
            "affects indications of detached HEADs (git-checkout[1]) and the raw diff \n" +
            "output (git-diff[1]). Printing an ellipsis in the cases mentioned is no longer \n" +
            "considered adequate and support for it is likely to be removed in the \n" +
            "foreseeable future (along with the variable). \n" +
            "More detail on the following is available from the Git concepts chapter of the \n" +
            "user-manual and gitcore-tutorial[7]. \n" +
            "A Git project normally consists of a working directory with a \".git\" \n" +
            "subdirectory at the top level. The .git directory contains, among other things, \n" +
            "a compressed object database representing the complete history of the \n" +
            "project, an \"index\" file which links that history to the current contents of the \n" +
            "working tree, and named pointers into that history such as tags and branch \n" +
            "heads. \n" +
            "The object database contains objects of three main types: blobs, which hold \n" +
            "file data; trees, which point to blobs and other trees to build up directory \n" +
            "hierarchies; and commits, which each reference a single tree and some \n" +
            "number of parent commits. \n" +
            "The commit, equivalent to what other systems call a \"changeset\" or \"version\", \n" +
            "represents a step in the project’s history, and each parent represents an \n" +
            "immediately preceding step. Commits with more than one parent represent \n" +
            "merges of independent lines of development. \n" +
            "All objects are named by the SHA-1 hash of their contents, normally written \n" +
            "as a string of 40 hex digits. Such names are globally unique. The entire history \n" +
            "leading up to a commit can be vouched for by signing just that commit. A \n" +
            "fourth object type, the tag, is provided for this purpose. \n" +
            "When first created, objects are stored in individual files, but for efficiency may \n" +
            "later be compressed together into \"pack files\". \n" +
            "Named pointers called refs mark interesting points in history. A ref may \n" +
            "contain the SHA-1 name of an object or the name of another ref. Refs with \n" +
            "names beginning \n" +
            "ref/head/ \n" +
            "contain the SHA-1 name of the most recent \n" +
            "commit (or \"head\") of a branch under development. SHA-1 names of tags of \n" +
            "interest are stored under \n" +
            "ref/tags/ \n" +
            ". A special ref named \n" +
            "contains the \n" +
            "name of the currently checked-out branch. \n" +
            "The index file is initialized with a list of all paths and, for each path, a blob \n" +
            "object and a set of attributes. The blob object represents the contents of the \n" +
            "file as of the head of the current branch. The attributes (last modified time, \n" +
            "size, etc.) are taken from the corresponding file in the working tree. \n" +
            "Subsequent changes to the working tree can be found by comparing these \n" +
            "attributes. The index may be updated with new content, and new commits \n" +
            "may be created from the content stored in the index. \n" +
            "The index is also capable of storing multiple entries (called \"stages\") for a \n" +
            "given pathname. These stages are used to hold the various unmerged version \n" +
            "of a file when a merge is in progress. \n" +
            "HEAD \n" +
            "FURTHER DOCUMENTATION \n" +
            "See the references in the \"description\" section to get started using Git. The \n" +
            "following is probably more detail than necessary for a first-time user. \n" +
            "The Git concepts chapter of the user-manual and gitcore-tutorial[7] both \n" +
            "provide introductions to the underlying Git architecture. \n" +
            "See gitworkflows[7] for an overview of recommended workflows. \n" +
            "See also the howto documents for some useful examples. \n" +
            "The internals are documented in the Git API documentation. \n" +
            "Users migrating from CVS may also want to read gitcvs-migration[7]. \n" +
            "Authors \n" +
            "Git was started by Linus Torvalds, and is currently maintained by Junio C \n" +
            "Hamano. Numerous contributions have come from the Git mailing list \n" +
            "<git@vger.kernel.org>. http://www.openhub.net/p/git/contributors/ \n" +
            "summary gives you a more complete list of contributors. \n" +
            "If you have a clone of git.git itself, the output of git-shortlog[1] and git- \n" +
            "blame[1] can show you the authors for specific parts of the project. \n" +
            "Reporting Bugs \n" +
            "Report bugs to the Git mailing list <git@vger.kernel.org> where the \n" +
            "development and maintenance is primarily done. You do not have to be \n" +
            "subscribed to the list to send a message there. See the list archive at https:// \n" +
            "public-inbox.org/git for previous bug reports and other discussions. \n" +
            "Issues which are security relevant should be disclosed privately to the Git \n" +
            "Security mailing list <git-security@googlegroups.com>. \n" +
            "\n" +
            "SEE ALSO \n" +
            "\n" +
            "gittutorial[7], gittutorial-2[7], giteveryday[7], gitcvs-migration[7], \n" +
            "    \n" +
            "gitglossary[7], gitcore-tutorial[7], gitcli[7], The Git User’s Manual, \n" +
            "    \n" +
            "gitworkflows[7] \n" +
            " \n" +
            "GIT \n" +
            "\n" +
            "Part of the git[1] suite ";

    public Text(String text) {
        this.text = text;
    }

    public Text() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Text{" +
                "text='" + text + '\'' +
                '}';
    }
}
