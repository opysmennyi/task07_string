public class Menu {
    int position;
    String command;

    public Menu(int position, String command) {
        this.position = position;
        this.command = command;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "position=" + position +
                ", command='" + command + '\'' +
                '}';
    }
}
